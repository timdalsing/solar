# Solar Panel IoT Demo

This repo contains a facsimile of an IoT system that collects data from solar panels, stores the data in an [Influx DB](https://www.influxdata.com/) time-series database, and displays the metrics in a web app.  No actual solar panels are used, so the collection of the data uses an emulator along with data that was captured for Machine Learning studies on [Kaggle](https://www.kaggle.com/code/shivrajanand/plant-1-solar-power-generation-task/).

The device emulator reads the CSV files downloaded from the Kaggle site.  For each line in each file a message is created.  The messages use [Google Protocol Buffers](https://developers.google.com/protocol-buffers) and are sent to the "data center" via [MQTT](https://mqtt.org/).  The message is received in the data center via MQTT and passed to [Kafka](https://kafka.apache.org/) via an adapter.  From there the messages are consumed from Kafka and stored in InfluxDB.

Two REST APIs are provided that allow the web app to read metadata and the actual power metrics.

The backend components are based on [Spring Boot](https://spring.io/projects/spring-boot).  [Spring Cloud Stream Kafka Binder](https://cloud.spring.io/spring-cloud-stream-binder-kafka/spring-cloud-stream-binder-kafka.html) is used for producing and consuming Kafka messages, [Spring Web](https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#spring-web) for the REST APIs, [InfluxDB Java Client](https://github.com/influxdata/influxdb-java) for accessing InfluxDB, and [Eclipse MQTT Java Client](https://www.eclipse.org/paho/index.php?page=clients/java/index.php) for consuming MQTT messages.  The MQTT broker is [Mosquitto](https://mosquitto.org/).

# Architecture

![Architecture](images/arch.jpg)

For this demo, the Solar Panel is emulated via a driver, which uses the MQTT Java client to publish messages to Mosquitto.  A MQTT/Kafka adapter that uses Spring Cloud Stream receives the MQTT message and converts it to a Kafka message.  The Kafka message is consumed by the Influx Consumer, which writes the data in InfluxDB.

Metadata Service and Influx Service are Spring Web REST APIs.  The web app retrieves the metadata and power metrics using these services.

# Web App

![Web App](images/solar.png)

The web app is a [ReactJS](https://reactjs.org/) application.