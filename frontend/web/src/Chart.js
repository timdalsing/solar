import React from "react"
import { observer } from "mobx-react"
import './App.css'
import { LineChart, Line, CartesianGrid, XAxis, YAxis, ResponsiveContainer, Legend, Tooltip } from 'recharts'

export const Chart = observer(({ queryResults }) => {
    return (
        <div className="Chart">
            <ResponsiveContainer width="80%" height="50%">
                <LineChart data={queryResults.results} margin={{ top: 25, right: 25, bottom: 50, left: 10 }} >
                    <Line type="monotone" dataKey="acPower" name="AC Power" stroke="green" />
                    <CartesianGrid stroke="#ccc" />
                    <XAxis dataKey="timestamp" angle={10} label="Timestamp" tickMargin={30}/>
                    <YAxis label="AC Power" orientation="left" width={200} unit="A"/>
                    <Tooltip />
                </LineChart>
            </ResponsiveContainer>
            <ResponsiveContainer width="80%" height="50%">
                <LineChart data={queryResults.results} margin={{ top: 25, right: 25, bottom: 50, left: 10 }} >
                    <Line type="monotone" dataKey="dcPower" name="DC Power" stroke="red" />
                    <CartesianGrid stroke="#ccc" />
                    <XAxis dataKey="timestamp" angle={10} label="Timestamp" tickMargin={30}/>
                    <YAxis label="DC Power" orientation="left" width={200} unit="A" />
                    <Tooltip />
                </LineChart>
            </ResponsiveContainer>
            <ResponsiveContainer width="80%" height="50%">
                <LineChart data={queryResults.results} margin={{ top: 25, right: 25, bottom: 50, left: 10 }} >
                    <Line type="monotone" dataKey="dailyYield" name="Daily Yield" stroke="magenta" />
                    <CartesianGrid stroke="#ccc" />
                    <XAxis dataKey="timestamp" angle={10} label="Timestamp" tickMargin={30}/>
                    <YAxis label="Daily Yield" orientation="left" width={200} unit="A" />
                    <Tooltip />
                </LineChart>
            </ResponsiveContainer>
            <ResponsiveContainer width="80%" height="50%">
                <LineChart data={queryResults.results} margin={{ top: 25, right: 25, bottom: 50, left: 10 }} >
                    <Line type="monotone" dataKey="totalyYield" name="Total Yield" stroke="blue" />
                    <CartesianGrid stroke="#ccc" />
                    <XAxis dataKey="timestamp" angle={10} label="Timestamp" tickMargin={30}/>
                    <YAxis label="Total Yield" orientation="left" width={200} unit="A" />
                    <Tooltip />
                </LineChart>
            </ResponsiveContainer>
        </div >
    )
})