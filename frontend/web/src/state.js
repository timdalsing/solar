import { makeAutoObservable } from "mobx"
import { metadata, influx } from "./api"

class DateRange {
    constructor() {
        const stop = new Date()
        const start = new Date()
        start.setFullYear(start.getFullYear() - 3)

        this.start = start
        this.stop = stop
        makeAutoObservable(this)
    }

    setStart(start) {
        this.start = start
    }

    setStop(stop) {
        this.stop = stop
    }
}

export const dateRangeStore = new DateRange()

class Plants {
    constructor() {
        this.plants = []
        makeAutoObservable(this)
    }

    setPlants(plants) {
        this.plants = plants
    }
}

export const plantsStore = new Plants()

class SelectedPlant {
    constructor() {
        this.plant = null
        makeAutoObservable(this)
    }

    setSelectedPlant(plant) {
        this.plant = plant
    }

    setSelectedPlantById(plantId) {
        const plants = plantsStore.plants
        const pl = plants.filter((plant) => plant.plantId === plantId)
        if (pl.length === 0) {
            this.plant = null
        } else {
            this.plant = pl[0]
        }
    }
}

export const selectedPlantStore = new SelectedPlant()

class SelectedDevice {
    constructor() {
        this.device = null
        makeAutoObservable(this)
    }

    setSelectedDevice(device) {
        this.device = device;
    }

    setSelectedDeviceById(deviceId) {
        const plant = selectedPlantStore.plant
        if (plant === null) {
            this.device = null
        } else {
            const dev = plant.devices.filter(device => device.sourceId === deviceId)
            if (dev.length === 0) {
                this.device = null
            } else {
                this.device = dev[0]
            }
        }
    }

}

export const selectedDeviceStore = new SelectedDevice()

export const retrievePlants = () => {
    metadata
        .get('/plant')
        .then(resp => {
            const data = resp.data
            plantsStore.setPlants(data)
        })
        .catch(err => console.log(`state.retrievePlants: err = ${JSON.stringify(err)}`))
}

class QueryResults {
    constructor() {
        this.results = []
        makeAutoObservable(this)
    }

    setResults(results) {
        this.results = results
    }
}

export const queryResultsStore = new QueryResults()

export const retrieveResults = (plantId, sourceId, startDate, stopDate) => {
    const url = `/power/${plantId}/${sourceId}/${startDate.toISOString()}/${stopDate.toISOString()}`
    influx
        .get(url)
        .then(resp => {
            const data = resp.data
            queryResultsStore.setResults(data)
        })
        .catch(err => console.log(`state.retrieveResults: err = ${JSON.stringify(err)}`))
}