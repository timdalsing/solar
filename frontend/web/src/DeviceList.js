import React from "react"
import { selectedDeviceStore } from "./state"
import { observer } from "mobx-react"
import './App.css'

export const DeviceList = observer(({ selectedPlant }) => {
    const selectDevice = (e) => {
        const deviceId = e.target.value === "" ? null : e.target.value
        selectedDeviceStore.setSelectedDeviceById(deviceId)
    }

    const plant = selectedPlant.plant
    if (plant === null) {
        return (
            <div>
                <select name="devices" disabled={true}>
                    <option value="">Please select a device...</option>
                </select>
            </div>
        )
    } else {
        return (
            <div style={{ display: "flex", flexDirection: "row" }}>
                <div style={{marginLeft: "10px", marginRight: "10px"}}>Device List: </div>
                <select name="devices" onChange={(e) => selectDevice(e)} className="DeviceList-select">
                    <option value="">Please select a device...</option>
                    {selectedPlant.plant.devices.map((device) => <option value={device.sourceId}>{device.sourceId}</option>)}
                </select>
            </div>
        )
    }
})
