import React from "react"
import { retrievePlants, selectedPlantStore } from "./state"
import { observer } from "mobx-react"
import './App.css'

retrievePlants()

export const PlantList = observer(({ plants }) => {
    const selectPlant = (e) => {
        const plantId = e.target.value === "" ? 0 : parseInt(e.target.value)
        selectedPlantStore.setSelectedPlantById(plantId)
    }

    return (
        <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ marginLeft: "10px", marginRight: "10px" }}>Plant List: </div>
            <select name="plants" onChange={(e) => selectPlant(e)} className="PlantList-select">
                <option value="">Please select a plant...</option>
                {plants.plants.map((plant) => <option value={plant.plantId}>{plant.plantId}</option>)}
            </select>
        </div>
    )
})
