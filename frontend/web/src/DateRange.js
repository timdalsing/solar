import React, { useState } from "react"
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import { dateRangeStore } from "./state"
import './App.css'

export const DateRange = () => {
    const [start, setStart] = useState(dateRangeStore.start)
    const [stop, setStop] = useState(dateRangeStore.stop)

    const selectStart = (date) => {
        setStart(date)
        dateRangeStore.setStart(date)
    }

    const selectStop = (date) => {
        setStop(date)
        dateRangeStore.setStop(date)
    }

    return (
        <div className="DateRange-container">
            <div style={{ marginLeft: "5px", marginRight: "5px"}}>Start: </div>
            <DatePicker selected={start} onChange={selectStart} onSelect={selectStart} />
            <div style={{ marginLeft: "5px", marginRight: "5px" }}>Stop: </div>
            <DatePicker selected={stop} onChange={selectStop} onSelect={selectStop} />
        </div>
    )
}