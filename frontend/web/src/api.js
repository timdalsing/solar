import axios from 'axios'

export const metadata = axios.create({
    baseURL: 'http://localhost:8735'
})

export const influx = axios.create({
    baseURL: 'http://localhost:8745'
})
