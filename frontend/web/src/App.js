import './App.css'
import { PlantList } from './PlantList'
import { DeviceList } from './DeviceList'
import { plantsStore, selectedPlantStore, selectedDeviceStore, queryResultsStore } from './state'
import { DateRange } from './DateRange'
import { ShowChart } from './ShowChart'
import { Chart } from './Chart'


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div className='App-title'>Solar Power</div>
        <div className='App-toolbar'>
          <PlantList plants={plantsStore} />
          <DeviceList selectedPlant={selectedPlantStore} />
          <DateRange />
          <ShowChart selectedPlant={selectedPlantStore} selectedDevice={selectedDeviceStore} />
        </div>
      </header>
      <div className='App-body'>
        <Chart queryResults={queryResultsStore} />
      </div>
    </div>
  );
}

export default App;
