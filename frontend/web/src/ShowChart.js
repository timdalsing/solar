import React from "react"
import { observer } from "mobx-react"
import './App.css'
import { retrieveResults, dateRangeStore } from "./state"

export const ShowChart = observer(({ selectedPlant , selectedDevice}) => {
    const isDisabled = selectedDevice.device === null

    const showChart = () => {
        const plantId = selectedPlant.plant.plantId
        const sourceId = selectedDevice.device.sourceId
        const start = dateRangeStore.start
        const stop = dateRangeStore.stop

        retrieveResults(plantId, sourceId, start, stop)
    }

    return (
        <div>
            <button onClick={showChart} disabled={isDisabled}>Show Chart</button>
        </div>
    )
})