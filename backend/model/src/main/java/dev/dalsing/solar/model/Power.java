package dev.dalsing.solar.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Power {

    int plantId;
    String sourceId;
    LocalDateTime timestamp;
    float acPower;
    float dcPower;
    float dailyYield;
    float totalYield;
}
