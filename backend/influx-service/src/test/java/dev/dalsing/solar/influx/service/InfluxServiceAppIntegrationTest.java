package dev.dalsing.solar.influx.service;

import com.influxdb.client.DeleteApi;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.WriteApi;
import com.influxdb.client.domain.DeletePredicateRequest;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import dev.dalsing.solar.influx.service.InfluxServiceApp.Key;
import dev.dalsing.solar.model.Power;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.SECONDS;

@SpringBootTest
class InfluxServiceAppIntegrationTest {

    @Autowired
    InfluxServiceApp app;

    @Autowired
    WriteApi writeApi;
    @Autowired
    DeleteApi deleteApi;

    @Autowired
    InfluxDBClient client;

    static Random random = new Random();

    String bucket = "solar";
    String org = "dalsing";
    String meas = "power";
    String[] sourceIds = {"source-0","source-1","source-2","source-3"};

    LocalDateTime now = LocalDateTime.now().truncatedTo(SECONDS);

    List<Point> points = new ArrayList<>();
    Map<Key, List<Power>> expected = new HashMap<>();

    @BeforeEach
    void setUp() throws Exception {
        points.clear();
        expected.clear();

        DeletePredicateRequest pred = new DeletePredicateRequest();
        pred.setStart(OffsetDateTime.now().minusYears(10));
        pred.setStop(OffsetDateTime.now().plusYears(10));
        deleteApi.delete(pred, bucket, org);

        now = LocalDateTime.now();
        LocalDateTime ts = now.minusDays(160).truncatedTo(SECONDS);

        for (int plantId = 0; plantId < 4; ++plantId) {
            for (int numSource = 0; numSource < 4; ++numSource) {
                String sourceId = sourceIds[numSource];
                Key key = Key.builder().plantId(plantId).sourceId(sourceId).build();
                List<Power> powerList = expected.computeIfAbsent(key, k -> new ArrayList<>());

                for (int numPoints = 0; numPoints < 10; ++numPoints) {
                    Point point = new Point(meas);

                    float acPower = random.nextFloat();
                    float dcPower = random.nextFloat();
                    float dailyYield = random.nextFloat();
                    float totalYield = random.nextFloat();

                    Power power = Power.builder()
                            .plantId(plantId)
                            .sourceId(sourceId)
                            .timestamp(ts)
                            .acPower(acPower)
                            .dcPower(dcPower)
                            .dailyYield(dailyYield)
                            .totalYield(totalYield)
                            .build();
                    powerList.add(power);

                    point.time(ts.toEpochSecond(ZoneOffset.UTC), WritePrecision.S);
                    point.addTag("plantId", "" + plantId);
                    point.addTag("sourceId", sourceId);

                    point.addField("acPower", acPower);
                    point.addField("dcPower", dcPower);
                    point.addField("dailyYield", dailyYield);
                    point.addField("totalYield", totalYield);

                    points.add(point);

                    ts = ts.plusDays(1).truncatedTo(SECONDS);
                }
            }

            writeApi.writePoints(bucket, org, points);
        }

        Thread.sleep(3000L); // for some reason the new points don't immediately show up in the database
    }

    @Tag("integration")
    @Test
    void getPowerForPlantAndSource_fullDateRange() {
        LocalDateTime start = now.minusDays(160).truncatedTo(SECONDS);
        LocalDateTime stop = now.truncatedTo(SECONDS);
        int plantId = 0;
        String sourceId = sourceIds[0];
        List<Power> expectedList = expected.get(Key.builder().plantId(plantId).sourceId(sourceId).build());

        Mono<List<Power>> mono = app.getPowerForPlantAndSource(plantId, sourceId, start, stop);
        StepVerifier.create(mono).assertNext(actual -> Assertions.assertThat(actual).containsExactlyInAnyOrderElementsOf(expectedList)).verifyComplete();
    }

    @Tag("integration")
    @Test
    void getPowerForPlantAndSource_partialDateRange() {
        LocalDateTime start = now.minusDays(160).truncatedTo(SECONDS);
        LocalDateTime stop = start.plusDays(10).truncatedTo(SECONDS);
        int plantId = 0;
        String sourceId = sourceIds[0];
        List<Power> expectedList = expected
                .entrySet()
                .stream()
                .flatMap(e -> e.getValue().stream())
                .filter(p -> (p.getTimestamp().isAfter(start) || p.getTimestamp().equals(start)) && p.getTimestamp().isBefore(stop) && p.getPlantId() == plantId && p.getSourceId().equals(sourceId))
                .collect(Collectors.toList());

        Mono<List<Power>> mono = app.getPowerForPlantAndSource(plantId, sourceId, start, stop);
        StepVerifier.create(mono).assertNext(actual -> Assertions.assertThat(actual).containsExactlyInAnyOrderElementsOf(expectedList)).verifyComplete();
    }
}