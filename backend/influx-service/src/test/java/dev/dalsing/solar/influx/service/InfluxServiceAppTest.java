package dev.dalsing.solar.influx.service;

import com.influxdb.client.QueryApi;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import dev.dalsing.solar.influx.common.InfluxProperties;
import dev.dalsing.solar.model.Power;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.List;

import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoUnit.SECONDS;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class InfluxServiceAppTest {

    InfluxServiceApp app;

    @Mock
    QueryApi queryApi;

    @BeforeEach
    void setUp() {
        app = new InfluxServiceApp(queryApi, new InfluxProperties());
    }

    @Test
    void getPowerForPlantAndSource() {
        int plantId = 1;
        String sourceId = "some-source";
        Float acPower = 1.23f;
        Float dcPower = 2.34f;
        Float dailyYield = 3.45f;
        Float totalYield = 4.56f;
        LocalDateTime now = LocalDateTime.now().truncatedTo(SECONDS);

        Power expected = Power.builder()
                .plantId(plantId)
                .sourceId(sourceId)
                .timestamp(now)
                .acPower(acPower)
                .dcPower(dcPower)
                .dailyYield(dailyYield)
                .totalYield(totalYield)
                .build();


        FluxTable fluxTable = new FluxTable();
        fluxTable.getRecords().add(createRecord(now, plantId, sourceId, "acPower", acPower));
        fluxTable.getRecords().add(createRecord(now, plantId, sourceId, "dcPower", dcPower));
        fluxTable.getRecords().add(createRecord(now, plantId, sourceId, "dailyYield", dailyYield));
        fluxTable.getRecords().add(createRecord(now, plantId, sourceId, "totalYield", totalYield));
        List<FluxTable> tableList = List.of(fluxTable);

        when(queryApi.query(anyString())).thenReturn(tableList);

        LocalDateTime stop = now;
        LocalDateTime start = stop.minusDays(1);

        Mono<List<Power>> mono = app.getPowerForPlantAndSource(plantId, sourceId, start, stop);

        StepVerifier.create(mono).assertNext(actual -> Assertions.assertThat(actual).containsOnly(expected)).verifyComplete();
    }

    private FluxRecord createRecord(LocalDateTime now, int plantId, String sourceId, String field, Float value) {
        FluxRecord fluxRecord = new FluxRecord(0);

        fluxRecord.getValues().put("_time", now.toInstant(UTC));
        fluxRecord.getValues().put("plantId", Integer.toString(plantId));
        fluxRecord.getValues().put("sourceId", sourceId);
        fluxRecord.getValues().put("_field", field);
        fluxRecord.getValues().put("_value", value.doubleValue());

        return fluxRecord;
    }
}