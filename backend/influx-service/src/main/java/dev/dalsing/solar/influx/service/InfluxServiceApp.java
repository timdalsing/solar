package dev.dalsing.solar.influx.service;

import com.influxdb.client.QueryApi;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import dev.dalsing.solar.influx.common.InfluxConfig;
import dev.dalsing.solar.influx.common.InfluxProperties;
import dev.dalsing.solar.model.Power;
import lombok.Builder;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.SECONDS;
import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@Import(InfluxConfig.class)
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class InfluxServiceApp {

    QueryApi queryApi;
    InfluxProperties influxProperties;

    public static void main(String[] args) {
        SpringApplication.run(InfluxServiceApp.class, args);
    }

    @Value
    @Builder
    static class Key {
        int plantId;
        String sourceId;
    }

    @GetMapping("/power/{plantId}/{sourceId}/{start}/{stop}")
    public Mono<List<Power>> getPowerForPlantAndSource(
            @PathVariable("plantId") int plantId,
            @PathVariable("sourceId") String sourceId,
            @PathVariable("start") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") LocalDateTime start,
            @PathVariable("stop") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") LocalDateTime stop) {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SS'Z'");
        String sstart = format.format(start);
        String sstop = format.format(stop);

        String query = String
                .format("from(bucket: \"solar\") |> range(start: %s, stop: %s) |> filter(fn: (r) => r.plantId == \"%d\" and r.sourceId == \"%s\") |> limit(n:100)",
                        sstart, sstop, plantId, sourceId);
        List<FluxTable> tableList = queryApi.query(query);
        Map<Key, List<Power>> map = convert(tableList);

        List<Power> list = map.get(Key.builder().plantId(plantId).sourceId(sourceId).build());
        return list == null ? Mono.empty() : Mono.just(list);
    }

    private Map<Key, List<Power>> convert(List<FluxTable> tableList) {
        List<FluxRecord> recordList = tableList
                .stream()
                .flatMap(table -> table.getRecords().stream()).collect(Collectors.toList());

        Map<Key, List<FluxRecord>> groupedRecords = recordList.stream()
                .collect(Collectors.groupingBy(rec -> Key.builder()
                        .plantId(Integer.parseInt((String) rec.getValueByKey("plantId")))
                        .sourceId((String) rec.getValueByKey("sourceId"))
                        .build()));

        return groupedRecords.entrySet().stream()
                .map(ek -> {
                    Key key = ek.getKey();

                    int pid = key.getPlantId();
                    String sid = key.getSourceId();

                    List<FluxRecord> records = ek.getValue();
                    List<Power> powerList = records.stream()
                            .collect(Collectors.groupingBy(FluxRecord::getTime)).entrySet().stream().map(et -> {
                                Instant instant = et.getKey();
                                LocalDateTime ts = LocalDateTime.ofInstant(instant, ZoneId.of("UTC")).truncatedTo(SECONDS);
                                return et.getValue().stream().reduce(Power.builder()
                                        .plantId(pid)
                                        .sourceId(sid)
                                        .timestamp(ts)
                                        .build(), this::populate, (pow1, pow2) -> pow1);
                            })
                            .collect(Collectors.toList());
                    return Pair.with(key, powerList);
                })
                .collect(Collectors.toMap(Pair::getValue0, Pair::getValue1));
    }

    private Power populate(Power power, FluxRecord record) {
        String field = record.getField();
        Double d = (Double) record.getValue();
        float value = d.floatValue();

        switch (field) {
            case "acPower":
                power.setAcPower(value);
                break;
            case "dcPower":
                power.setDcPower(value);
                break;
            case "dailyYield":
                power.setDailyYield(value);
                break;
            case "totalYield":
                power.setTotalYield(value);
                break;
        }

        return power;
    }
}
