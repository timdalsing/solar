package dev.dalsing.solar.mqtt.source;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;

import static lombok.AccessLevel.PRIVATE;

@Component
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class StreamBridgeWrapper {

    StreamBridge bridge;

    public boolean send(String bindingName, Object data, MimeType outputContentType) {
        return bridge.send(bindingName, data, outputContentType);
    }
}
