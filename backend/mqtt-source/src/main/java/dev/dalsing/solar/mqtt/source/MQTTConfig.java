package dev.dalsing.solar.mqtt.source;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.MimeType;

import java.util.UUID;

import static lombok.AccessLevel.PRIVATE;

@Configuration
@EnableConfigurationProperties(AppProperties.class)
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class MQTTConfig {

    AppProperties appProperties;
    StreamBridgeWrapper bridge;

    private static final MimeType mimeType = MimeType.valueOf("application/x-protobuf");

    @Bean
    public MqttClient mqttClient() throws Exception {
        String id = UUID.randomUUID().toString();

        MqttConnectOptions options = new MqttConnectOptions();

        options.setAutomaticReconnect(appProperties.isAutoReconnect());
        options.setCleanSession(appProperties.isCleanSession());
        options.setConnectionTimeout(appProperties.getConnectionTimeout());
        options.setKeepAliveInterval(appProperties.getKeepAliveInterval());
        options.setMaxInflight(appProperties.getMaxInflight());

        MqttClient mqttClient = new MqttClient(appProperties.getUri(), id);
        mqttClient.connect(options);

        mqttClient.subscribe(appProperties.getTopic(), (topic, msg) -> {
            byte[] payload = msg.getPayload();
            bridge.send("power-out-0", payload, mimeType);
        });

        return mqttClient;
    }
}
