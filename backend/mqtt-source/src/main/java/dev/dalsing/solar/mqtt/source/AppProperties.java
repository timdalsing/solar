package dev.dalsing.solar.mqtt.source;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static org.eclipse.paho.client.mqttv3.MqttConnectOptions.*;

@ConfigurationProperties("app")
@Data
public class AppProperties {

    String uri = "tcp://localhost:1883";
    String topic = "power";
    boolean autoReconnect = true;
    boolean cleanSession = true;
    int connectionTimeout = CONNECTION_TIMEOUT_DEFAULT;
    int keepAliveInterval = KEEP_ALIVE_INTERVAL_DEFAULT;
    int maxInflight = MAX_INFLIGHT_DEFAULT;
}
