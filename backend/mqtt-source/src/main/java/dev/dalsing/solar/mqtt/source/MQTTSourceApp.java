package dev.dalsing.solar.mqtt.source;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.function.StreamBridge;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
public class MQTTSourceApp {

    public static void main(String[] args) {
        SpringApplication.run(MQTTSourceApp.class, args);
    }
}
