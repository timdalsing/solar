package dev.dalsing.solar.mqtt.source;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;

import static lombok.AccessLevel.PRIVATE;

@Component
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class MQTTListener implements IMqttMessageListener {

    StreamBridgeWrapper bridge;

    private static final MimeType mimeType = MimeType.valueOf("application/x-protobuf");

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        byte[] payload = message.getPayload();
        bridge.send("power-out-0", payload, mimeType);
    }
}
