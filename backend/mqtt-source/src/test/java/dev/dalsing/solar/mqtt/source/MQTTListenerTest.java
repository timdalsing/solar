package dev.dalsing.solar.mqtt.source;

import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.MimeType;

import java.nio.charset.StandardCharsets;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class MQTTListenerTest {

    MQTTListener listener;

    @Mock
    StreamBridgeWrapper bridge;

    @BeforeEach
    void setUp() {
        listener = new MQTTListener(bridge);
    }

    @Test
    void messageArrived() throws Exception {
        String topic = "some-topic";
        byte[] payload = "some-payload".getBytes(StandardCharsets.UTF_8);
        MqttMessage message = new MqttMessage(payload);
        listener.messageArrived(topic, message);

        verify(bridge).send("power-out-0", payload, MimeType.valueOf("application/x-protobuf"));
    }
}