package dev.dalsing.solar.influx.consumer;

import com.influxdb.client.WriteApi;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import dev.dalsing.solar.influx.common.InfluxConfig;
import dev.dalsing.solar.influx.common.InfluxProperties;
import dev.dalsing.solar.pb.PBPower;
import dev.dalsing.solar.pb.common.PowerMessageConverter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.util.function.Consumer;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@Import({InfluxConfig.class, PowerMessageConverter.class})
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class InfluxConsumerApp {

    WriteApi writeApi;
    InfluxProperties influxProperties;

    public static void main(String[] args) {
        SpringApplication.run(InfluxConsumerApp.class, args);
    }

    @Bean
    public Consumer<Flux<PBPower>> consumer() {
        String bucket = influxProperties.getBucket();
        String org = influxProperties.getOrg();

        return flux -> flux
                .map(pb ->
                        Point
                                .measurement("power")
                                .time(Instant.ofEpochSecond(pb.getTimestamp()), WritePrecision.S)
                                .addTag("plantId", Integer.toString(pb.getPlantId()))
                                .addTag("sourceId", pb.getSourceId())

                                .addField("acPower", ((Float) pb.getAcPower()).doubleValue())
                                .addField("dcPower", ((Float) pb.getDcPower()).doubleValue())
                                .addField("dailyYield", ((Float) pb.getDailyYield()).doubleValue())
                                .addField("totalYield", ((Float) pb.getTotalYield()).doubleValue())

                )
                .subscribe(point -> writeApi.writePoint(bucket, org, point));
    }
}
