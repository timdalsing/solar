package dev.dalsing.solar.influx.consumer;

import com.influxdb.client.WriteApi;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import dev.dalsing.solar.influx.common.InfluxProperties;
import dev.dalsing.solar.pb.PBPower;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;

import java.time.Instant;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class InfluxConsumerAppTest {

    InfluxConsumerApp app;

    @Mock
    WriteApi writeApi;

    @Captor
    ArgumentCaptor<Point> captor;

    @BeforeEach
    void setUp() {
        app = new InfluxConsumerApp(writeApi, new InfluxProperties());
    }

    @Test
    void consumer_success() {
        int plantId = 2;
        String sourceId = "some-source-id";
        long ts = 4L;
        Float acPower = 12.34f;
        Float dcPower = 34.56f;
        Float dailyYield = 23.45f;
        Float totalYield = 56.78f;

        String bucket = "solar";
        String org = "dalsing";

        PBPower power = PBPower.newBuilder()
                .setPlantId(plantId)
                .setSourceId(sourceId)
                .setTimestamp(ts)
                .setAcPower(acPower)
                .setDcPower(dcPower)
                .setDailyYield(dailyYield)
                .setTotalYield(totalYield)
                .build();

        Flux<PBPower> flux = Flux.just(power);
        app.consumer().accept(flux);

        verify(writeApi).writePoint(eq(bucket), eq(org), captor.capture());

        String expected = Point
                .measurement("power")
                .time(Instant.ofEpochSecond(ts), WritePrecision.S)
                .addTag("plantId", Integer.toString(plantId))
                .addTag("sourceId", sourceId)
                .addField("acPower", acPower.doubleValue())
                .addField("dcPower", dcPower.doubleValue())
                .addField("dailyYield", dailyYield.doubleValue())
                .addField("totalYield", totalYield.doubleValue())
                .toLineProtocol();

        String actual = captor.getValue().toLineProtocol();

        Assertions.assertThat(actual).isEqualTo(expected);
    }
}