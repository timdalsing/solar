package dev.dalsing.solar.influx.consumer;

import com.influxdb.client.DeleteApi;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.domain.DeletePredicateRequest;
import com.influxdb.query.FluxRecord;
import dev.dalsing.solar.pb.PBPower;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Slf4j
class InfluxConsumerAppIntegrationTest {

    @Autowired
    InfluxConsumerApp app;

    @Autowired
    InfluxDBClient client;

    static Random random = new Random();

    @BeforeEach
    void setUp() {
        DeleteApi deleteApi = client.getDeleteApi();
        DeletePredicateRequest pred = new DeletePredicateRequest();
        pred.setStart(OffsetDateTime.now().minusYears(10));
        pred.setStop(OffsetDateTime.now().plusYears(10));
        deleteApi.delete(pred, "solar", "dalsing");
    }

    @Tag("integration")
    @Test
    void consumer_success() throws Exception {
        float acPower = 12.34f;
        float dailyYield = 23.45f;
        float dcPower = 34.56f;
        int plantId = random.nextInt(1000);
        String sourceId = UUID.randomUUID().toString();
        float totalYield = 56.78f;
        long ts = LocalDate.now().toEpochSecond(LocalTime.parse("00:00:00.000"), ZoneOffset.UTC);

        PBPower expected = PBPower.newBuilder()
                .setAcPower(acPower)
                .setDailyYield(dailyYield)
                .setDcPower(dcPower)
                .setPlantId(plantId)
                .setSourceId(sourceId)
                .setTotalYield(totalYield)
                .setTimestamp(ts)
                .build();

        Flux<PBPower> flux = Flux.just(expected);

        app.consumer().accept(flux);

        Thread.sleep(3000L); // to let the database catchup

        List<FluxRecord> recordList = client
                .getQueryApi()
                .query("from(bucket: \"solar\") |> range(start: 0)")
                .stream()
                .flatMap(table -> table.getRecords().stream())
                .collect(Collectors.toList());

        Assertions.assertThat(recordList).hasSize(4);

        double actualAcPower = recordList.stream().filter(rec -> "acPower".equals(rec.getField())).map(rec -> (Double) rec.getValue()).mapToDouble(t -> t).distinct().findFirst().getAsDouble();
        double actualDcPower = recordList.stream().filter(rec -> "dcPower".equals(rec.getField())).map(rec -> (Double) rec.getValue()).mapToDouble(t -> t).distinct().findFirst().getAsDouble();
        double actualDailyYield = recordList.stream().filter(rec -> "dailyYield".equals(rec.getField())).map(rec -> (Double) rec.getValue()).mapToDouble(t -> t).distinct().findFirst().getAsDouble();
        double actuaTotalYield = recordList.stream().filter(rec -> "totalYield".equals(rec.getField())).map(rec -> (Double) rec.getValue()).mapToDouble(t -> t).distinct().findFirst().getAsDouble();

        assertThat(actualAcPower).isEqualTo(acPower);
        assertThat(actualDcPower).isEqualTo(dcPower);
        assertThat(actualDailyYield).isEqualTo(dailyYield);
        assertThat(actuaTotalYield).isEqualTo(totalYield);
    }
}