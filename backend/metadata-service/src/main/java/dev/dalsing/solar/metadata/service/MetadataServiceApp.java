package dev.dalsing.solar.metadata.service;

import dev.dalsing.solar.model.Device;
import dev.dalsing.solar.model.Plant;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.sql.ResultSet;
import java.util.List;
import java.util.stream.Collectors;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class MetadataServiceApp {

    JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        SpringApplication.run(MetadataServiceApp.class, args);
    }

    @GetMapping("/plant")
    public Mono<List<Plant>> getAllPlants() {
        List<Pair<Integer, String>> pairs = jdbcTemplate
                .query("select p.plantId, d.sourceId from plant p join device d on p.plantId = d.plantId",
                        (row, rowNum) -> convert(row));
        List<Plant> plants = convert(pairs);
        return plants.isEmpty() ? Mono.empty() : Mono.fromSupplier(() -> plants);
    }

    @GetMapping("/plant/{plantId}")
    public Mono<Plant> getPlantForId(@PathVariable("plantId") int plantId) {
        List<Pair<Integer, String>> pairs = jdbcTemplate
                .query("select p.plantId, d.sourceId from plant p join device d on p.plantId = d.plantId where p.plantId = ?",
                        (row, rowNum) -> convert(row), plantId);
        // TODO more than 1 in result set
        List<Plant> plants = convert(pairs);
        return plants.isEmpty() ? Mono.empty() : Mono.fromSupplier(() -> plants.get(0));
    }

    private List<Plant> convert(List<Pair<Integer, String>> pairs) {
        return pairs.stream()
                .collect(Collectors.groupingBy(Pair::getValue0))
                .entrySet().stream().map(e -> {
                    List<Device> devices = e.getValue().stream()
                            .map(p -> Device.builder().sourceId(p.getValue1()).build())
                            .collect(Collectors.toList());
                    return Plant.builder().plantId(e.getKey()).devices(devices).build();
                })
                .collect(Collectors.toList());
    }

    private Pair<Integer, String> convert(ResultSet rs) {
        try {
            return Pair.with(rs.getInt(1), rs.getString(2));
        } catch (Exception e) {
            throw new IllegalArgumentException(e.toString(), e);
        }
    }
}
