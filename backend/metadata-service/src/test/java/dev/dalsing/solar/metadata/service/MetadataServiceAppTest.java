package dev.dalsing.solar.metadata.service;

import dev.dalsing.solar.model.Device;
import dev.dalsing.solar.model.Plant;
import org.javatuples.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MetadataServiceAppTest {

    MetadataServiceApp app;

    @Mock
    JdbcTemplate jdbcTemplate;

    int plantId1 = 1;
    int plantId2 = 2;
    String sourceId1 = "source-1";
    String sourceId2 = "source-2";
    String sourceId3 = "source-3";
    String sourceId4 = "source-4";

    @BeforeEach
    void setUp() {
        app = new MetadataServiceApp(jdbcTemplate);
    }

    @Test
    void getAllPlants() {
        List<Plant> expected = List.of(
                Plant.builder()
                        .plantId(plantId1)
                        .devices(List.of(
                                Device.builder().sourceId(sourceId1).build(),
                                Device.builder().sourceId(sourceId2).build()))
                        .build(),
                Plant.builder()
                        .plantId(plantId2)
                        .devices(List.of(Device.builder().sourceId(sourceId3).build(),
                                Device.builder().sourceId(sourceId4).build()))
                        .build());

        List<Pair<Integer, String>> pairs = List.of(
                Pair.with(plantId1, sourceId1),
                Pair.with(plantId1, sourceId2),
                Pair.with(plantId2, sourceId3),
                Pair.with(plantId2, sourceId4));

        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(pairs);

        Mono<List<Plant>> actual = app.getAllPlants();

        StepVerifier.create(actual).assertNext(p -> assertThat(p).containsExactlyInAnyOrderElementsOf(expected)).verifyComplete();
    }

    @Test
    void getPlantForId() {
        Plant expected = Plant.builder()
                .plantId(plantId1)
                .devices(List.of(
                        Device.builder().sourceId(sourceId1).build(),
                        Device.builder().sourceId(sourceId2).build()))
                .build();

        List<Pair<Integer, String>> pairs = List.of(
                Pair.with(plantId1, sourceId1),
                Pair.with(plantId1, sourceId2));
        when(jdbcTemplate.query(anyString(), any(RowMapper.class), any())).thenReturn(pairs);

        Mono<Plant> actual = app.getPlantForId(plantId1);

        StepVerifier.create(actual).assertNext(p -> assertThat(expected)).verifyComplete();
    }
}