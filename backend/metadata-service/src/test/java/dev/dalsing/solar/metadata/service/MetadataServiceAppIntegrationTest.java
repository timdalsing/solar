package dev.dalsing.solar.metadata.service;

import dev.dalsing.solar.model.Device;
import dev.dalsing.solar.model.Plant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class MetadataServiceAppIntegrationTest {

    @Autowired
    MetadataServiceApp app;

    @Autowired
    JdbcTemplate jdbcTemplate;

    int plantId1 = 1;
    int plantId2 = 2;
    String sourceId1 = "source-1";
    String sourceId2 = "source-2";
    String sourceId3 = "source-3";
    String sourceId4 = "source-4";

    List<Plant> plants = List.of(
            Plant.builder()
                    .plantId(plantId1)
                    .devices(List.of(
                            Device.builder().sourceId(sourceId1).build(),
                            Device.builder().sourceId(sourceId2).build()))
                    .build(),
            Plant.builder()
                    .plantId(plantId2)
                    .devices(List.of(Device.builder().sourceId(sourceId3).build(),
                            Device.builder().sourceId(sourceId4).build()))
                    .build());

    @BeforeEach
    void setUp() {
        jdbcTemplate.update("delete from device");
        jdbcTemplate.update("delete from plant");

        for (Plant plant : plants) {
            jdbcTemplate.update("insert into plant (plantId) values (?)", plant.getPlantId());

            for (Device device: plant.getDevices()) {
                jdbcTemplate.update("insert into device (sourceId, plantId) values (?,?)", device.getSourceId(),
                        plant.getPlantId());
            }
        }
    }

    @Tag("integration")
    @Test
    void getAllPlants() {
        Mono<List<Plant>> listMono = app.getAllPlants();
        StepVerifier.create(listMono).assertNext(actual -> assertThat(actual).isEqualTo(plants)).verifyComplete();
    }

    @Tag("integration")
    @Test
    void getPlantForId() {
        Mono<Plant> plantMono = app.getPlantForId(plantId1);
        StepVerifier.create(plantMono).assertNext(actual -> assertThat(actual).isEqualTo(plants.get(0))).verifyComplete();
    }
}