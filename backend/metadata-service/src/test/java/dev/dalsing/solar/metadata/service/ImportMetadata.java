package dev.dalsing.solar.metadata.service;

import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootTest
@Disabled
@Slf4j
public class ImportMetadata {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Disabled
    @Test
    void importData() throws Exception {
        File rootDir = new File("/Users/tdalsing/workspace/projects/solar/misc/data/plant");
        File[] files = rootDir.listFiles(file -> file.getName().toLowerCase().endsWith(".csv"));

        List<Pair<Integer, String>> pairs = new ArrayList<>();

        for(File file: files) {
            try (FileReader fileReader = new FileReader(file)) {
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                bufferedReader.readLine(); // move past header

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    String[] fields = line.split(",");

                    int plantId = Integer.parseInt(fields[1]);
                    String sourceId = fields[2];

                    Pair<Integer, String> pair = Pair.with(plantId, sourceId);
                    pairs.add(pair);
                }
            }
        }

        Map<Integer, List<Pair<Integer, String>>> collect = pairs.stream().collect(Collectors.groupingBy(p -> p.getValue0()));

        jdbcTemplate.update("delete from device");
        jdbcTemplate.update("delete from plant");

        for (Map.Entry<Integer, List<Pair<Integer, String>>> e: collect.entrySet()) {
            Integer plantId = e.getKey();
            List<Pair<Integer, String>> value = e.getValue();
            List<String> sourceIds = value.stream().map(p -> p.getValue1()).distinct().collect(Collectors.toList());

            log.info("plantId = {}, value.size() = {}, sourceIds.size() = {}", plantId, value.size(), sourceIds.size());

            jdbcTemplate.update("insert into plant (plantId) values (?)", plantId);

            for (String sourceId: sourceIds) {
                jdbcTemplate.update("insert into device (sourceId, plantId) values (?,?)", sourceId, plantId);
            }
        }
    }
}
