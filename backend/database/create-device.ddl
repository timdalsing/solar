create table device (
    sourceId varchar(40) primary key,
    plantId int not null,
    constraint plant_fk
          foreign key(plantId)
    	  references plant(plantId)
);

alter table device owner to test;

create index table_plant_ndx on device (plantId);