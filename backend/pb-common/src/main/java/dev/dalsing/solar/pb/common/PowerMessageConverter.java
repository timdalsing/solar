package dev.dalsing.solar.pb.common;

import dev.dalsing.solar.pb.PBPower;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.AbstractMessageConverter;
import org.springframework.messaging.converter.MessageConversionException;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;

@Component
public class PowerMessageConverter extends AbstractMessageConverter {

    public PowerMessageConverter() {
        super(MimeType.valueOf("application/x-protobuf"));
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(PBPower.class);
    }

    @Override
    protected Object convertFromInternal(Message<?> message, Class<?> targetClass, Object conversionHint) {
        try {
            byte[] data = (byte[])message.getPayload();
            return PBPower.parseFrom(data);
        } catch (Exception e) {
            throw new MessageConversionException(e.toString(), e);
        }
    }

    @Override
    protected Object convertToInternal(Object payload, MessageHeaders headers, Object conversionHint) {
        PBPower power = (PBPower) payload;
        return power.toByteArray();
    }
}
