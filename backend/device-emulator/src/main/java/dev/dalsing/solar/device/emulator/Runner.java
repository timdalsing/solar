package dev.dalsing.solar.device.emulator;

import com.google.common.util.concurrent.RateLimiter;
import dev.dalsing.solar.pb.PBPower;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicInteger;

import static java.time.ZoneOffset.UTC;
import static lombok.AccessLevel.PRIVATE;

@Component
@Profile("!test")
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class Runner implements CommandLineRunner {

    StreamBridge bridge;
    AtomicInteger count = new AtomicInteger(0);
    RateLimiter rateLimiter = RateLimiter.create(1000.0);
    private static final MimeType mimeType = MimeType.valueOf("application/x-protobuf");

    @Override
    public void run(String... args) throws Exception {
        if (args.length != 1) {
            throw new IllegalArgumentException("Usage: <data dir>");
        }

        String sdir = args[0];
        File rootDir = new File(sdir);

        if (!rootDir.exists() || !rootDir.isDirectory()) {
            throw new IllegalArgumentException("Data directory "+rootDir.getAbsolutePath()+" does not exist or is not a directory");
        }

        File[] files = rootDir.listFiles(file -> file.getName().toLowerCase().endsWith(".csv"));

        if (files.length == 0) {
            throw new IllegalArgumentException("Data directory "+rootDir.getAbsolutePath()+" does not contain any CSV files");
        }

        for (File file: files) {
            read(file);
        }
    }

    private void read(File file) throws Exception {
        try (FileReader fileReader = new FileReader(file)) {
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            bufferedReader.readLine(); // move past header

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] fields = line.split(",");

                LocalDateTime ts = LocalDateTime.parse(fields[0], formatter);

                PBPower pbPower = PBPower.newBuilder()
                        .setPlantId(Integer.parseInt(fields[1]))
                        .setSourceId(fields[2])
                        .setTimestamp(ts.toEpochSecond(UTC))
                        .setDcPower(Float.parseFloat(fields[3]))
                        .setAcPower(Float.parseFloat(fields[4]))
                        .setDailyYield(Float.parseFloat(fields[5]))
                        .setTotalYield(Float.parseFloat(fields[6]))
                        .build();

                rateLimiter.acquire(1);
                bridge.send("power-out-0", pbPower, mimeType);

                if (count.incrementAndGet() % 1000 == 0) {
                    log.info("read: count = {}", count.get());
                }
            }
        }
    }
}
