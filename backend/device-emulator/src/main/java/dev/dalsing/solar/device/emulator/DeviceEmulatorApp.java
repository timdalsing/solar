package dev.dalsing.solar.device.emulator;

import dev.dalsing.solar.pb.common.PowerMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(PowerMessageConverter.class)
public class DeviceEmulatorApp {

    public static void main(String[] args) {
        SpringApplication.run(DeviceEmulatorApp.class, args);
    }
}
