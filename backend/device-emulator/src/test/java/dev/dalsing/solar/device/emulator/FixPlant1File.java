package dev.dalsing.solar.device.emulator;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.stream.Collectors;

@Disabled
class FixPlant1File {

    @Disabled
    @Test
    void fixFile() throws Exception {
        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        File rootDir = new File("/Users/tdalsing/workspace/projects/solar/misc/data/plant/raw");

        File inputFile = new File(rootDir, "Plant_1_Generation_Data.csv");
        File outputFile = new File(rootDir, "Plant_1_Generation_Data_Fixed.csv");

        FileReader inputReader = new FileReader(inputFile);
        FileWriter outputWriter = new FileWriter(outputFile);

        BufferedReader inputBuf = new BufferedReader(inputReader);
        BufferedWriter outputBuf = new BufferedWriter(outputWriter);

        String line = inputBuf.readLine();
        outputBuf.write(line);
        outputBuf.write('\n');

        while((line = inputBuf.readLine()) != null) {
            String[] fields = line.split(",");
            String inputDate = fields[0];
            LocalDateTime ts = LocalDateTime.parse(inputDate, inputFormatter);
            String outputDate = outputFormatter.format(ts);
            fields[0] = outputDate;
            String outputLine = Arrays.stream(fields).collect(Collectors.joining(","));
            outputBuf.write(outputLine);
            outputBuf.write('\n');
        }
    }
}