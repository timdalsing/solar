package dev.dalsing.solar.influx.common;

import com.influxdb.client.*;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(InfluxProperties.class)
public class InfluxConfig {

    @Bean
    public InfluxDBClient client(InfluxProperties appProperties) {
        return InfluxDBClientFactory.create(appProperties.getUrl(), appProperties.getToken().toCharArray(), appProperties.getOrg());
    }

    @Bean
    public WriteApi writeApi(InfluxDBClient client) {
        return client.makeWriteApi();
    }

    @Bean
    public DeleteApi deleteApi(InfluxDBClient client) {
        return client.getDeleteApi();
    }

    @Bean
    public QueryApi queryApi(InfluxDBClient client) {
        return client.getQueryApi();
    }
}
