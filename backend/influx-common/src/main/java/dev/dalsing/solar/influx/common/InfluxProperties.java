package dev.dalsing.solar.influx.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("influx")
@Data
public class InfluxProperties {

    String url = "http://localhost:8086";
    String token = "GjtFkNa6DV_kmOw85ZpqQva3NX55WJH1OgvFlNDL9vM4__2dJp7slxTsTYmUQTUghCT8VpdLwiWigRgmGGLJKQ==";
    String bucket = "solar";
    String org = "dalsing";
}
